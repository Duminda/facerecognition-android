#include <DetectionBasedTracker_jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/contrib/detection_based_tracker.hpp>
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>


#include <android/log.h>

#define LOG_TAG "FaceDetection/DetectionBasedTracker"
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

using namespace std;
using namespace cv;

inline void vector_Rect_to_Mat(vector<Rect>& v_rect, Mat& mat)
{
    mat = Mat(v_rect, true);
}

JNIEXPORT jlong JNICALL Java_com_dimi_afrs_DetectionBasedTracker_nativeCreateObject
(JNIEnv * jenv, jclass, jstring jFileName, jint faceSize)
{
    const char* jnamestr = jenv->GetStringUTFChars(jFileName, NULL);
    string stdFileName(jnamestr);
    jlong result = 0;

    try
    {
    DetectionBasedTracker::Parameters DetectorParams;
    if (faceSize > 0)
        DetectorParams.minObjectSize = faceSize;
    result = (jlong)new DetectionBasedTracker(stdFileName, DetectorParams);
    }
    catch(cv::Exception e)
    {
    LOGD("nativeCreateObject catched cv::Exception: %s", e.what());
    jclass je = jenv->FindClass("org/opencv/core/CvException");
    if(!je)
        je = jenv->FindClass("java/lang/Exception");
    jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
    LOGD("nativeCreateObject catched unknown exception");
    jclass je = jenv->FindClass("java/lang/Exception");
    jenv->ThrowNew(je, "Unknown exception in JNI code {highgui::VideoCapture_n_1VideoCapture__()}");
    return 0;
    }

    return result;
}

JNIEXPORT void JNICALL Java_com_dimi_afrs_DetectionBasedTracker_nativeSetFaceSize
(JNIEnv * jenv, jclass, jlong thiz, jint faceSize)
{
    try
    {
    if (faceSize > 0)
    {
        DetectionBasedTracker::Parameters DetectorParams = \
        ((DetectionBasedTracker*)thiz)->getParameters();
        DetectorParams.minObjectSize = faceSize;
        ((DetectionBasedTracker*)thiz)->setParameters(DetectorParams);
    }

    }
    catch(cv::Exception e)
    {
    LOGD("nativeStop catched cv::Exception: %s", e.what());
    jclass je = jenv->FindClass("org/opencv/core/CvException");
    if(!je)
        je = jenv->FindClass("java/lang/Exception");
    jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
    LOGD("nativeSetFaceSize catched unknown exception");
    jclass je = jenv->FindClass("java/lang/Exception");
    jenv->ThrowNew(je, "Unknown exception in JNI code {highgui::VideoCapture_n_1VideoCapture__()}");
    }
}


JNIEXPORT jint JNICALL Java_com_dimi_afrs_DetectionBasedTracker_Find
(JNIEnv* env,jclass obj,jstring jHaarCascadeFileName,jstring jCVSFileName, jlong imageGray, jlong faces_out,jint predictionValue)
{
	try{

		const char* jcascadenamestr = env->GetStringUTFChars(jHaarCascadeFileName, NULL);
		string stdHaarFileName(jcascadenamestr);
		const char* jcvsnamestr = env->GetStringUTFChars(jCVSFileName, NULL);
		string stdCVSFileName(jcvsnamestr);

		LOGD("HaarCascade = %s | CVSFile = %s ",stdHaarFileName.c_str(),stdCVSFileName.c_str());

		// Get the path to your CSV:
		//jstring fn_haar = jstring("haarcascade_frontalface_default.xml");
		//jstring fn_csv = jstring("csv.ext");
		int deviceId = 0;


		// These vectors hold the images and corresponding labels:
		vector<Mat> images;
		vector<jint> labels;

		std::ifstream file(stdCVSFileName.c_str(), ifstream::in);

	    if (!file) {

	        LOGD("No valid input file was given, please check the given filename");
	    }

		string line, path, classlabel;
		while (getline(file, line)) {
			stringstream liness(line);
			getline(liness, path, ';');
			getline(liness, classlabel);
			if(!path.empty() && !classlabel.empty()) {
				images.push_back(imread(path, 0));
				labels.push_back(atoi(classlabel.c_str()));
			}
		}

		// Get the height from the first image. We'll need this
		// later in code to reshape the images to their original
		// size AND we need to reshape incoming faces to this size:
		int im_width = images[0].cols;
		int im_height = images[0].rows;
		// Create a FaceRecognizer and train it on the given images:
		Ptr<FaceRecognizer> model = createEigenFaceRecognizer();//changed..


/*		Mat W = model->eigenvectors();
	    for (int i = 0; i < min(10, W.cols); i++) {
	        // get eigenvector #i
	        Mat ev = W.col(i).clone();
	        // reshape to original size AND normalize between [0...255]
	        Mat grayscale = toGrayscale(ev.reshape(1, im_height));
	        // show image (with Jet colormap)
	        Mat cgrayscale;
	        applyColorMap(grayscale, cgrayscale, COLORMAP_JET);
	        imshow(format("%d", i), cgrayscale);
	    }*/

		model->train(images, labels);
		// That's it for learning the Face Recognition model. You now
		// need to create the classifier for the task of Face Detection.
		// We are going to use the haar cascade you have specified in the
		// command line arguments:
		//
		CascadeClassifier haar_cascade;
		haar_cascade.load(stdHaarFileName);
		// Get a handle to the Video device:
		//LOGD("Get a handle to the Video device");
		//VideoCapture cap(deviceId);
		// Check if we can use this device at all:
		//if(!cap.isOpened()) {
			//LOGD("Can't Open Device ID = %d",deviceId);
			//return -1;
		//}
		// Holds the current frame from the Video device:
		//Mat frame;
		for(;;) {
			//cap >> frame;
			// Clone the current frame:
			//Mat original = frame.clone();
			// Convert the current frame to grayscale:
			//Mat gray;
			//cvtColor(original, gray, CV_BGR2GRAY);
			// Find the faces in the frame:
			vector< Rect_<jint> > faces;
	
			Mat gray=*((Mat*)imageGray);

			haar_cascade.detectMultiScale(gray, faces);
			// At this point you have the position of the faces in
			// faces. Now we'll get the faces, make a prediction and
			// annotate it in the video. Cool or what?
			for(jint i = 0; i < faces.size(); i++) {
				// Process face by face:
				Rect face_i = faces[i];
				// Crop the face from the image. So simple with OpenCV C++:
				Mat face = gray(face_i);
				// Resizing the face is necessary for Eigenfaces and Fisherfaces. You can easily
				// verify this, by reading through the face recognition tutorial coming with OpenCV.
				// Resizing IS NOT NEEDED for Local Binary Patterns Histograms, so preparing the
				// input data really depends on the algorithm used.
				//
				// I strongly encourage you to play around with the algorithms. See which work best
				// in your scenario, LBPH should always be a contender for robust face recognition.
				//
				// Since I am showing the Fisherfaces algorithm here, I also show how to resize the
				// face you have just found:
				Mat face_resized;
				cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
				// Now perform the prediction, see how easy that is:

				vector_Rect_to_Mat(faces, *((Mat*)faces_out));

				double predicted_confidence = 0.0;
				int prediction;

				model->predict(face_resized,prediction,predicted_confidence);
				
				
				// And finally write all we've found out to the original image!
				// First of all draw a green rectangle around the detected face:
				//rectangle(original, face_i, CV_RGB(0, 255,0), 1);
				// Create the text we will annotate the box with:
				//string box_text = string("Prediction = "+ prediction);
				// Calculate the position for annotated text (make sure we don't
				// put illegal values in there):
				int pos_x = std::max(face_i.tl().x - 10, 0);
				int pos_y = std::max(face_i.tl().y - 10, 0);
				// And now put it into the image:
				LOGD("Prediction = %d Predicted Confidence = %Lf",prediction,predicted_confidence);
				predictionValue=prediction;
				//putText(original, box_text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0,255,0), 2.0);
				if(prediction>=0)
					return predictionValue;
				
			}

		}
		return predictionValue;
		}catch(cv::Exception e){
			LOGD("nativeCreateObject catched cv::Exception: %s", e.what());
			jclass je = env->FindClass("org/opencv/core/CvException");
			if(!je)
			je = env->FindClass("java/lang/Exception");
			env->ThrowNew(je, e.what());
		}
		catch (...)
		{
		LOGD("nativeCreateObject catched unknown exception");
		jclass je = env->FindClass("java/lang/Exception");
		env->ThrowNew(je, "Unknown exception in JNI code {highgui::VideoCapture_n_1VideoCapture__()}");
		}
	}

