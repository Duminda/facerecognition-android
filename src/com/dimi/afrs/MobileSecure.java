package com.dimi.afrs;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;


public class MobileSecure extends ListActivity{
	
	private static final boolean DEBUG = true;
	private static final String TAG = "MobileSecure";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	if (DEBUG)	Log.i(TAG, "============> MobileSecure.onCreate()");
    	super.onCreate(savedInstanceState);

    	boolean isLogined;		
    	boolean isLocked;		
    	
    	Intent iLockService = new Intent(this,LockService.class);
    	SharedPreferences sharedData = getSharedPreferences(LoginActivity.PREF, 0);
    	
    	isLogined = sharedData.getBoolean(LoginActivity.PREF_LOGIN, false);
    	isLocked  = sharedData.getBoolean(LoginActivity.PREF_LOCK, false);
    	
    	if(isLogined){
    		
    		if(LockActivity.isLocked){
				Intent unlockIntent = new Intent("com.dimi.afrs.UNLOCK");
				unlockIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				unlockIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(unlockIntent);
				LockActivity.isLocked = false;
			}
    	}
    	else{
    		Intent intent = new Intent(this,LockActivity.class);
			startActivity(intent);
			finish();
    	}
    	
    	if(isLocked){
    		startService(iLockService);
    	}
    	else{
    		stopService(iLockService);
    	}
    	
    	if (!isLogined) {
     		Intent intent = new Intent(this, LoginActivity.class);
    		startActivity(intent);
    		finish();
    	}
    	else {
    		
    	}
    	
    	}
    }



