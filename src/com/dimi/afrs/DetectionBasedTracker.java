package com.dimi.afrs;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;

public class DetectionBasedTracker
{	
	public DetectionBasedTracker(String cascadeName, int minFaceSize)
	{
		mNativeObj = nativeCreateObject(cascadeName, minFaceSize);
	}

	public void setMinFaceSize(int size)
	{
		nativeSetFaceSize(mNativeObj, size);
	}
	

	
	//Added by Duminda
	public int Recognize(String cascade,String csv,Mat imageGray, MatOfRect faces,int prediction)
	{
		return Find(cascade, csv, imageGray.getNativeObjAddr(), faces.getNativeObjAddr(),prediction);
	}
	
	private long mNativeObj = 0;
	//private int recoFactor = 0;//added by Duminda
	
	private static native long nativeCreateObject(String cascadeName, int minFaceSize);
	private static native void nativeSetFaceSize(long thiz, int size);
	private static native int Find(String cascade, String csv, long inputImage, long faces,int prediction);//Added by Duminda
	
	static
	{
		System.loadLibrary("detection_based_tracker");
	}
}
