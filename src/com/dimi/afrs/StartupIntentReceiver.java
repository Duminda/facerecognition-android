package com.dimi.afrs;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class StartupIntentReceiver extends BroadcastReceiver{
	@Override
	public void onReceive(Context context, Intent intent) {
	
	     if (intent.getAction().equals(Intent.ACTION_SCREEN_ON))
	      {    
	 		Intent mBootIntent = new Intent(context,BringToFront.class);
			context.startActivity(mBootIntent);

	      }
	     
         
		Intent mBootIntent = new Intent(context,MobileSecure.class);
		mBootIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(mBootIntent);
	}
}