package com.dimi.afrs;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class LockService extends Service{

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	@Override
	public void onCreate(){
		super.onCreate();
	}
	@Override
	public void onStart(Intent intent,int startId){
		super.onStart(intent, startId);
		startActivity();
	}
	public void startActivity(){
		Intent i = new Intent(this,LockActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}
}
