package com.dimi.afrs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

public class OCVCamBase extends Activity {
    private static final String TAG            = "Sample::Activity";

    private MenuItem            mItemPreviewRGBA;
    private MenuItem            mItemPreviewGray;
    private OCVCamView mView;
    Button captureButton;
    Button quitButton;
    
    
    UserCreate uc;
    public static File working_Dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/FRSUser/");
    static File fileC;
    static {
    	working_Dir.mkdirs();
    	 fileC = new File(OCVCamBase.working_Dir,"csv.txt");
    
    }

    public OCVCamBase() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    @Override
	protected void onPause() {
        Log.i(TAG, "onPause");
        if (mView.mCamera != null)
			
		{
        mView.mCamera.stopPreview();//Newly Added
		mView.mCamera.release();//Added
		mView.mCamera=null;//Added
		
		}		
		if (mView != null)
			
		{
		mView=null;

		}
		super.onPause();
		//mView.releaseCamera();
	}

	@Override
	protected void onResume() {
        Log.i(TAG, "onResume");
		super.onResume();
		if( !mView.openCamera() ) {
			AlertDialog ad = new AlertDialog.Builder(this).create();  
			ad.setCancelable(false); // This blocks the 'BACK' button  
			ad.setMessage("Fatal error: can't open camera!");  
			ad.setButton("OK", new DialogInterface.OnClickListener() {  
			    public void onClick(DialogInterface dialog, int which) {  
				dialog.dismiss();
				finish();
			    }  
			});  
			ad.show();
		}
	}

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        mView = new OCVCamView(this);
        
        setContentView(R.layout.normalcam);  
        ((FrameLayout) findViewById(R.id.preview)).addView(mView);
        
        captureButton=(Button) findViewById(R.id.buttonClick);
        captureButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
						   mView.mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
					
			}
		});
        
        quitButton=(Button) findViewById(R.id.buttonQuit);
        quitButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//mView.releaseCamera();
				//mView=null;

				//mView.surfaceDestroyed(OCVCamViewBase.mHolder);
				
				backtoprivious();
			}
		});
       
        //setContentView(mView);
    }
    
	private void backtoprivious() {
		// TODO Auto-generated method stub
		if(mView.mCamera!=null){
		mView.mCamera.stopPreview();
		mView.mCamera.release();
		mView.mCamera = null;
		}
		
		Intent intent = new Intent(this, UserCreate.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
    
    ShutterCallback shutterCallback=new ShutterCallback() {
		
		public void onShutter() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onShutter'd");
		}
	};
	
	int i=0;
	
	PictureCallback jpegCallback=new PictureCallback() {
		
		public void onPictureTaken(byte[] data, Camera arg1) {
			// TODO Auto-generated method stub
			FileOutputStream outStream = null;
			
			try {
				outStream = new FileOutputStream(working_Dir+""+i+".jpg");
				outStream.write(data);
				outStream.close();
				Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length+" Image: "+i+".pgm");
				i++;
		  		Toast.makeText(OCVCamBase.this, "Train Image Saved ", Toast.LENGTH_SHORT).show();
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
			}
			Log.d(TAG, "onPictureTaken - jpeg");
		}
	};
	
	
	PictureCallback rawCallback=new PictureCallback() {
		
		public void onPictureTaken(byte[] arg0, Camera arg1) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onPictureTaken - raw");
		}
	};
	
	

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu");
        mItemPreviewRGBA = menu.add("Preview RGBA");
        mItemPreviewGray = menu.add("Preview GRAY");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "Menu Item selected " + item);
        if (item == mItemPreviewRGBA)
        	mView.setViewMode(OCVCamView.VIEW_MODE_RGBA);
        else if (item == mItemPreviewGray)
        	mView.setViewMode(OCVCamView.VIEW_MODE_GRAY);
        return true;
    }
    
}
