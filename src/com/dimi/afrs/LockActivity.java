/*
 * Logs:
 * 2010-04-11:	1. 从TestLock.java和TestFullScreen.java移植过来
 * 2010-04-12:	1. 接收短信指令后，正常unlock了！
 * 				2. add onKeyDown()
 * 
 * ToDo:
 * 1. 直接在这里面进行unlock
 */
package com.dimi.afrs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class LockActivity extends Activity {
	
	private static final boolean DEBUG = false;
	private static final String TAG = "LockActivity";
	static boolean isLocked;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);   	
    	if (DEBUG)	Log.i(TAG, "============> " + TAG + ".onCreate()");
    	        
    	requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,   
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
        setContentView(R.layout.lock);
        isLocked = true;
    }
    @Override
    protected void onNewIntent (Intent intent) {
    	super.onNewIntent(intent);
    	if (DEBUG)	Log.i(TAG, "============> " + TAG + ".onNewIntent()");
    	
    	if (intent.getAction().equals("com.dimi.afrs.UNLOCK")) {
    		isLocked = false;
    		finish();
    		Log.i(TAG, "unlock success!");
    	}
    }
   
    @Override
    public void onPause(){
    	super.onPause();
    }
    @Override
    public void onStop(){
    	super.onStop();
    	if(isLocked){
    		Log.i("LockActivity","isLocked and onStop");
    		finish();
    		Intent i = new Intent(this,LockService.class);
    		startService(i);
    	}
    	SharedPreferences settings = getSharedPreferences("MOBILESECURE",0);
    	settings.edit().putBoolean(LoginActivity.PREF_LOCK,isLocked).commit();
    }
}