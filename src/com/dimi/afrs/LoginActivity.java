
package com.dimi.afrs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.os.Environment;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class LoginActivity extends Activity {

	public static final String PREF_LOGIN = "isLogined";
	public static final String PREF_LOCK = "isLocked";
	public static final String PREF = "MOBILESECURE";

	

    private ImageView imageView;
    private ImageView imageView2;


    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);     
        setTitle("[ Advance FR Security ]");
        
        findViews();
        initButtons();
        //createCSVFile();
    }
    

    private void findViews() {
    	imageView = (ImageView) findViewById(R.id.imageView1);
    	imageView2 = (ImageView) findViewById(R.id.imageView2);

    }
    
    private void initButtons() {
    	imageView.setOnClickListener(new OnClickListener() {  
            public void onClick(View arg0) {
            	 startFaceRecognition() ;
            		//Toast.makeText(LoginActivity.this, "Login Data Send !", Toast.LENGTH_SHORT)	// LoginActivity.this
           		 	//	 .show();
            		
            		
    				
            	//} else {
            		//Toast.makeText(LoginActivity.this, "Login Data not Sent!!!", Toast.LENGTH_SHORT)
      		 		 	// .show();
            	//}
            }
        });
    	
    	
    	imageView2.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				
				startSettings();
				
			}
		});
    }
    

    
    public void startFaceRecognition() {
    	
		Intent i = new Intent(this,FdActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
    }
    
    
    public void startSettings(){
    	
    	//Begin SettingsPermission Activity
		Intent i = new Intent(this,SettingsPermission.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
    }
    

}






