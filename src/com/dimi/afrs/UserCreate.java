package com.dimi.afrs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserCreate extends Activity {

    private Button capture;
    private Button setuser;
    private EditText uname;
    private EditText pwd;
    private EditText repwd;
    //public String usrname;
    //public String password;
    //public String repassword;
    
    //private ProgressDialog dialog;
    
    int i=0;
    


	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.adduser);
		

		capture = (Button) findViewById(R.id.capturebtn);
		uname = (EditText) findViewById(R.id.unametxt);
		pwd = (EditText) findViewById(R.id.pwdtxt);
		repwd = (EditText) findViewById(R.id.repwdtxt);
		

		setuser = (Button) findViewById(R.id.setusrbtn);
		

		
		capture.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				
				//launch camera
				//get shots
				//save in /sdcard/name/1.jpg
			
						   startCamera();
				

			}


		});
		
		setuser.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				saveData();
			}


		});
	}

	private void startCamera() {
		// TODO Auto-generated method stub
		//usrname=uname.getText().toString();
		
		Intent i=new Intent(this, OCVCamBase.class);
		startActivity(i);
	}
	
	
	private void saveData() {
		// TODO Auto-generated method stub
		//imgView.getDrawingCache();
	    String usrname=uname.getText().toString();
	    String password=pwd.getText().toString();
	    String repassword=repwd.getText().toString();
	     
		if(password.equals(repassword)){
        DBUserAdapter db=new DBUserAdapter(UserCreate.this);

        long rowId=db.addUser(usrname,password);
        if(rowId>0){
            Toast.makeText(getApplicationContext(), "New Phone User Saved !",
                    Toast.LENGTH_LONG).show();
                    Log.e("save", "New Phone User Saved");	
        }
		}
		else{
        Toast.makeText(getApplicationContext(), "Both Passwords should match !",
        Toast.LENGTH_LONG).show();
        Log.e("pwd", "passwords conflict");
		}
		
		createCSVFile();
		
		
	}
	
    public void createCSVFile(){
    	
    	File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/FRSUser/");
    	ListDir(root);
    }
    
    public void ListDir(File f){
    	
    	
    	String FileName=Environment.getExternalStorageDirectory().getAbsolutePath()+"/FRSUser/csv.txt";
    	try {
    	FileWriter outFile = new FileWriter(FileName);
		PrintWriter out = new PrintWriter(outFile);
		
        File[] files = f.listFiles();
       // fileList.clear();
        for (File file : files){
         //fileList.add(file.getPath()); 
        	out.println(file.getPath()+";"+uname.getText().toString());
        }
        }catch (IOException e){
			e.printStackTrace();
        }

    }


}
