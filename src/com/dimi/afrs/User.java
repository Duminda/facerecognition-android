package com.dimi.afrs;

import android.graphics.Bitmap;

public class User {

		private long id;
		private String uname;
		private String pwd;
		private Bitmap uimage;

		public User(String uname,String pwd)
		{
			this.uname=uname;
			this.pwd=pwd;
			//this.uimage=uimage;
		}
		
		public User(String uname)
		{
			this.uname=uname;
	
		}
		
		public User() {
			// TODO Auto-generated constructor stub
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getUserName() {
			return uname;
		}

		public void setUserName(String uname) {
			this.uname = uname;
		}
		
		public String getPassword() {
			return pwd;
		}

		public void setPassword(String pwd) {
			this.pwd = pwd;
		}
		
		public Bitmap getUserImage() {
			return uimage;
		}

		public void setUserImage(Bitmap uimage) {
			this.uimage = uimage;
		}

		// Will be used by the ArrayAdapter in the ListView
		@Override
		public String toString() {
			return uname;
		}
	

}
