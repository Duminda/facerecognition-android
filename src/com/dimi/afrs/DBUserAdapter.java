package com.dimi.afrs;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBUserAdapter 
{ 
    public static final String KEY_ROWID = "_id";
    public static final String KEY_USERNAME= "username";
    public static final String KEY_PASSWORD = "password";
    //public static final String KEY_IMAGE = "image";
    public static final String TABLE = "users";
    
    private static final String TAG = "DBAdapter";
    
    private static final String DATABASE_NAME = "usersdb";
    private static final String DATABASE_TABLE = "users";
    private static final int DATABASE_VERSION = 1;
    
    private String[] allColumns = { KEY_ROWID,KEY_USERNAME,KEY_PASSWORD };

    private static final String DATABASE_CREATE =
        "create table users(_id integer primary key autoincrement, "
        + "username text not null, " 
        + "password text not null);";
        
    private Context context = null;  
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    
    public DBUserAdapter(Context ctx) 
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }
        
    private static class DatabaseHelper extends SQLiteOpenHelper 
    {
        DatabaseHelper(Context context) 
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) 
        {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion 
                    + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS users");
            onCreate(db);
        }
    }    
    
    
    public void open() throws SQLException 
    {
        db = DBHelper.getWritableDatabase();
    }

      
    public void close() 
    {
        DBHelper.close();
    }    
    
    public long addUser(String username, String password)
    {
    	open();
    	//User user=new User();
    	
    	//ByteArrayOutputStream out = new ByteArrayOutputStream();
		//user.getUserImage().compress(Bitmap.CompressFormat.PNG, 100, out);
		User newuser=new User(username);
		long id = newuser.getId();
		
    	 ContentValues initialValues = new ContentValues();
    	 
    	 //initialValues.put(KEY_ROWID, "3");
         initialValues.put(KEY_USERNAME, username);
         initialValues.put(KEY_PASSWORD, password);
         //initialValues.put(KEY_IMAGE, image); 
         
         return  db.insert(DATABASE_TABLE, null, initialValues);
         


    }
    
	public long deleteUser(String uname) {
		open();
		User newuser=new User(uname);
		long id = newuser.getId();
		System.out.println("User deleted with id: " + id);
		return db.delete(DATABASE_TABLE,KEY_USERNAME + " =  ?", new String[] { String.valueOf(uname) });
	}

    public boolean Login(String username, String password) throws SQLException 
    {
    	Cursor mCursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE + " WHERE username=? AND password=?", new String[]{username,password});
        if (mCursor != null) {           
            if(mCursor.getCount() > 0)
            {
            	return true;
            }
        }
     return false;
    }
    
    public boolean RetriveUserName(String username) throws SQLException 
    {
    	Cursor mCursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE + " WHERE username=?", new String[]{username});
        if (mCursor != null) {           
            if(mCursor.getCount() > 0)
            {
            	return true;
            }
        }
     return false;
    }
    
    public List<User> getAllComments() {
		List<User> unames = new ArrayList<User>();

		Cursor cursor = db.query(KEY_USERNAME,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			User uname = cursorToUser(cursor);
			unames.add(uname);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return unames;
	}

	private User cursorToUser(Cursor cursor) {
		User uname = new User();
		uname.setId(cursor.getLong(0));
		uname.setUserName(cursor.getString(1));
		return uname;
	}
	



}
