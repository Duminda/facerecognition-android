package com.dimi.afrs;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserDelete extends Activity{
	
	private Button deluserbutton;
    private EditText deletetxt;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
    	super.onCreate(savedInstanceState);
		setContentView(R.layout.deleteuser);
		
    	deluserbutton=(Button) findViewById(R.id.deletebtn);
    	deletetxt=(EditText) findViewById(R.id.delusertxt);
    	
    	deluserbutton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				deleteUser();
			}
		});
    }
    
    private void deleteUser()
    {
    	String uname;
    	
    	uname=deletetxt.getText().toString();
        
		if(!uname.equals("")){
        DBUserAdapter db=new DBUserAdapter(this);

		long row=db.deleteUser(uname);
		if(row>0){
	        Toast.makeText(getApplicationContext(), "User "+uname+" Deleted !",
            Toast.LENGTH_LONG).show();
            Log.e("pwd", "user deleted");
		}
		}
		else{
        Toast.makeText(getApplicationContext(), "User to delete ?",
        Toast.LENGTH_LONG).show();
        Log.e("pwd", "user to deleted empty");
		}
	}
       
 }

