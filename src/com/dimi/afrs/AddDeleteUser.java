package com.dimi.afrs;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class AddDeleteUser extends TabActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Put your code here
		setTitle("[ Add Delete Users ]");
		setContentView(R.layout.settingsmain);
		
		Resources resources = getResources(); 
		TabHost tabHost = getTabHost(); 

		Intent addUser=new Intent().setClass(this,UserCreate.class);
		TabSpec uAdd=tabHost
				  .newTabSpec("UserAdd")
				  .setIndicator("",resources.getDrawable(R.drawable.adduser))
				  .setContent(addUser);
		
		Intent deleteUser=new Intent().setClass(this,UserDelete.class);
		TabSpec uDel=tabHost
				  .newTabSpec("UserDelete")
				  .setIndicator("",resources.getDrawable(R.drawable.deleteuser))
				  .setContent(deleteUser);
		
		
		tabHost.addTab(uAdd);
		tabHost.addTab(uDel);
		
		tabHost.setCurrentTab(0);
			

	}

	
	
}
