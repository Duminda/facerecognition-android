package com.dimi.afrs;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.Toast;

public class FdActivity extends Activity {
    private static final String TAG         = "Sample::Activity";

    private MenuItem            mItemFace50;
    private MenuItem            mItemFace40;
    private MenuItem            mItemFace30;
    private MenuItem            mItemFace20;
    private MenuItem            mItemType;
    
    Button enter_button;
    String pname;
    
    private FdView		mView;
    
    
    private BaseLoaderCallback  mOpenCVCallBack = new BaseLoaderCallback(this) {
    	@Override
    	public void onManagerConnected(int status) {
    		switch (status) {
				case LoaderCallbackInterface.SUCCESS:
				{
					Log.i(TAG, "OpenCV loaded successfully");
					
					// Load native libs after OpenCV initialization
					System.loadLibrary("detection_based_tracker");
					
					
					setContentView(R.layout.preview);
					// Create and set View
					mView = new FdView(mAppContext);
					mView.setDetectorType(mDetectorType);
					mView.setMinFaceSize(0.2f);
					
					//setContentView(R.layout.preview);
					((FrameLayout) findViewById(R.id.preview_frame)).addView(mView);
					findViewById(R.id.preview_frame).setVisibility(View.VISIBLE);
							 
			        ButtonListener listener=new ButtonListener();
			        enter_button=((Button) findViewById(R.id.button_go));
			        enter_button.setVisibility(View.VISIBLE);
			        enter_button.setOnClickListener(listener);
			        

			        enter_button=((Button) findViewById(R.id.button_quit));
			        enter_button.setVisibility(View.VISIBLE);
			        enter_button.setOnClickListener(listener);
					
/*					mView.setClickable(true);
					FRSListner listener=new FRSListner();
					mView.setOnClickListener(listener);
				
					//setContentView(mView);
					mView.setClickable(true);
					FRSListner listener=new FRSListner();
					mView.setOnClickListener(listener);
					
*/					// Check native OpenCV camera
					if( !mView.openCamera() ) {
						AlertDialog ad = new AlertDialog.Builder(mAppContext).create();
						ad.setCancelable(false); // This blocks the 'BACK' button
						ad.setMessage("Fatal error: can't open camera!");
						ad.setButton("OK", new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						    }
						});
						ad.show();
					}
				} break;
				default:
				{
					super.onManagerConnected(status);
				} break;
			}
    	}
    };
    
    

    private class ButtonListener implements View.OnClickListener
    {

    	
		public void onClick(View view) {
			// TODO Auto-generated method stub
			
	    	if(mView.predictionVal==1){
				 pname="Duminda";
	    	}
			else{
				 pname="Unknown";
			}
			
			if(view.equals(findViewById(R.id.button_go)))
			{
				//Toast.makeText(FdActivity.this, "Enter Password for "+pname, Toast.LENGTH_SHORT).show();
				continueValidation();
			}
			
			if(view.equals(findViewById(R.id.button_quit)))
			{
				//Toast.makeText(FdActivity.this, "Enter Password for "+pname, Toast.LENGTH_SHORT).show();
				backtoprivious();
			}
		}

    }
    
	private void continueValidation() {
		// TODO Auto-generated method stub
		if(mView.predictionVal==1){
			 pname="Duminda";
		}
		DBUserAdapter db=new DBUserAdapter(this);
		if(pname!=null){
		if(db.RetriveUserName(pname)){
			openOptionsMenu();
			
/*			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);*/
			
		}else{
			Toast.makeText(FdActivity.this, "User "+pname+" does not have permissions !", Toast.LENGTH_SHORT).show();
		}
		}
		else{
			
			if (mView.mCamera != null)
				
			{
			mView.mCamera.release();//Added
			mView.mCamera=null;//Added
			
			}		
			if (mView != null)
				
			{
			mView=null;
			}
			
			Intent intent = new Intent(this, LoginActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
	}

	

	private void backtoprivious() {
		// TODO Auto-generated method stub
		//mView = new FdView(getBaseContext());

		
		if (mView.mCamera != null)
			
		{
		mView.mCamera.release();//Added
		mView.mCamera=null;//Added
		
		}		
		if (mView != null)
			
		{
		mView=null;
		}
		
		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

    private int                 mDetectorType = 0;
    private String[]            mDetectorName; 

    public FdActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
        mDetectorName = new String[2];
        mDetectorName[FdView.JAVA_DETECTOR] = "Java";
        mDetectorName[FdView.NATIVE_DETECTOR] = "Native (tracking)";
    }

	@Override
	protected void onPause() {
        Log.i(TAG, "onPause");
		
		if (mView.mCamera != null)
			
		{
			
		mView.mCamera.release();//Added
		mView.mCamera=null;//Added
		
		}		
		if (mView != null)
			
		{
		mView=null;
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        
		
		
		if( mView != null && !mView.openCamera() ) {
			AlertDialog ad = new AlertDialog.Builder(this).create();  
			ad.setCancelable(false); // This blocks the 'BACK' button  
			ad.setMessage("Fatal error: can't open camera!");  
			ad.setButton("OK", new DialogInterface.OnClickListener() {  
			    public void onClick(DialogInterface dialog, int which) {  
			        dialog.dismiss();                      
					finish();
			    }  
			});  
			ad.show();
		}
		
	}

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Log.i(TAG, "Trying to load OpenCV library");
        if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this, mOpenCVCallBack))
        {
        	Log.e(TAG, "Cannot connect to OpenCV Manager");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu");
        mItemFace50 = menu.add("Face size 50%");
        mItemFace40 = menu.add("Face size 40%");
        mItemFace30 = menu.add("Face size 30%");
        mItemFace20 = menu.add("Face size 20%");
        mItemType   = menu.add(mDetectorName[mDetectorType]);
        		
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "Menu Item selected " + item);
        if (item == mItemFace50)
            mView.setMinFaceSize(0.5f);
        else if (item == mItemFace40)
        	mView.setMinFaceSize(0.4f);
        else if (item == mItemFace30)
        	mView.setMinFaceSize(0.3f);
        else if (item == mItemFace20)
        	mView.setMinFaceSize(0.2f);
        else if (item == mItemType)
        {
        	mDetectorType = (mDetectorType + 1) % mDetectorName.length;
        	item.setTitle(mDetectorName[mDetectorType]);
        	mView.setDetectorType(mDetectorType);
        }
        return true;
    }

}
