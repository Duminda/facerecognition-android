package com.dimi.afrs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsPermission extends Activity {
	
	private Button buttonChangeSettings;
	private EditText userName;
	private EditText password;
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Put your code here
		setTitle("[ Authentication Required ! ]");
		setContentView(R.layout.permission);
		getAdminPermission();
	}
	
	public void getAdminPermission(){
		
		buttonChangeSettings=(Button)findViewById(R.id.button1)	;
		userName=(EditText)findViewById(R.id.editText1)	;
		password=(EditText)findViewById(R.id.editText2)	;
		

		buttonChangeSettings.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String uname=userName.getText().toString();
				String pwd=password.getText().toString();
				
				try{
					if(uname.length() > 0 && pwd.length() >0)
					{
						DBUserAdapter dbUser = new DBUserAdapter(SettingsPermission.this);
						dbUser.open();
						dbUser.addUser("Duminda", "123");
						if(dbUser.Login(uname, pwd))
						{
							Toast.makeText(SettingsPermission.this,"Successfully Logged In", Toast.LENGTH_LONG).show();
							startAddDeleteUserActivity();
						}else{
							Toast.makeText(SettingsPermission.this,"Invalid Username/Password", Toast.LENGTH_LONG).show();
						}
						dbUser.close();
					}
					
				}catch(Exception e)
				{
					Toast.makeText(SettingsPermission.this,e.getMessage(), Toast.LENGTH_LONG).show();
				}

				
			}


		});
	}
	
	private void startAddDeleteUserActivity() {
		// TODO Auto-generated method stub
		Intent i = new Intent(this,AddDeleteUser.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
		
	}
	
	
}
